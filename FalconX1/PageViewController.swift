//
//  PageViewController.swift
//  FalconX1
//
//  Created by Бесханум Гасанова on 27.08.2022.
//

import UIKit

class PageViewController: UIPageViewController {

    //MARK: - Variable
    var spaces = [SpaceHelper]()
    let firstSpaceImage = UIImage(named: "ракета")
    let secondSpaceImage = UIImage(named: "ракета 2")
    let thirdSpaceImage = UIImage(named: "ракета 3")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let firstSpace = SpaceHelper(name: "Space - ракета", image: firstSpaceImage!)
        let secondSpace = SpaceHelper(name: "Space - ракета 2", image: secondSpaceImage!)
        let thirdSpace = SpaceHelper(name: "Space - ракета 3", image: thirdSpaceImage!)
        delegate = self
        dataSource = self
        spaces.append(firstSpace)
        spaces.append(secondSpace)
        spaces.append(thirdSpace)
    }
    
    //MARK: - create vc
  lazy var arraySpaceViewController: [ViewController] = {
        var spaceVC = [ViewController]()
      for space in spaces {
          spaceVC.append(ViewController(spaceWith: space))
      }
       return spaceVC
    }()
      
    //MARK: - init UIPageViewController
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: navigationOrientation, options: nil)
        setViewControllers([arraySpaceViewController[0]], direction: .forward, animated: true, completion: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension PageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? ViewController else { return nil }
        if let index = arraySpaceViewController.firstIndex(of: viewController) {
            if index > 0 {
                return arraySpaceViewController[index - 1]
            }
        } else {
            return nil
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? ViewController else { return nil }
        if let index = arraySpaceViewController.firstIndex(of: viewController) {
            if index < spaces.count - 1 {
                return arraySpaceViewController[index + 1]
            }
        }
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return spaces.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
