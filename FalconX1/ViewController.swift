//
//  ViewController.swift
//  FalconX1
//
//  Created by Бесханум Гасанова on 25.08.2022.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - image
    private let spaceImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
 
    //MARK: - label
    private let nameLabel: UILabel = {
      let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var subView: [UIView] = [self.spaceImage, self.nameLabel]
    
    //MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    init(spaceWith: SpaceHelper){
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = UIColor.black
        edgesForExtendedLayout = []
        spaceImage.image = spaceWith.image
        nameLabel.text = spaceWith.name
        for view in subView { self.view.addSubview(view) }
        
        spaceImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        spaceImage.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        spaceImage.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        spaceImage.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: spaceImage.bottomAnchor, constant: 10).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: spaceImage.leftAnchor, constant: 5).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

